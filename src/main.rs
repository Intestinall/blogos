#![no_std] // don't link the Rust standard library
#![no_main] // disable all Rust-level entry points

use core::panic::PanicInfo;
use crate::color::Color;

pub mod color;
mod utils;

static HELLO: &str = "Ceci est mon chibre. Prenez-le, Rust me l'a offert!";
static HELLO2: &str = "Ou peut-etre pas.";

#[no_mangle]
pub extern "C" fn _start() -> ! {
    // this function is the entry point, since the linker looks for a function
    // named `_start` by default

    let mut vga_buffer = 0xb8000 as *mut u8;

    utils::write_to_vga_buffer(vga_buffer, HELLO, Color::BrightBlue);

    loop {}
}

/// This function is called on panic.
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
