pub enum Color {
    Black,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Brown,
    Gray,
    DarkGray,
    BrightBlue,
    BrightGreen,
    BrightCyan,
    BrightRed,
    BrightMagenta,
    Yellow,
    White,
}

impl Color {
    pub fn bright(self) -> Color {
        match self {
            Color::Blue => Color::BrightBlue,
            Color::Green => Color::BrightGreen,
            Color::Cyan => Color::BrightCyan,
            Color::Red => Color::BrightRed,
            Color::Magenta => Color::BrightMagenta,
            Color::Brown => Color::Yellow,
            Color::Black => Color::DarkGray,
            Color::DarkGray => Color::Gray,
            Color::Gray => Color::White,
            _ => self
        }
    }

    pub fn dark(self) -> Color {
        match self {
            Color::BrightBlue => Color::Blue,
            Color::BrightGreen => Color::Green,
            Color::BrightCyan => Color::Cyan,
            Color::BrightRed => Color::Red,
            Color::BrightMagenta => Color::Magenta,
            Color::Yellow => Color::Brown,
            Color::White => Color::Gray,
            Color::Gray => Color::DarkGray,
            Color::DarkGray => Color::Black,
            _ => self
        }
    }

    pub fn to_bytes(&self) -> u8 {
        match self {
            Color::Black => 0x0,
            Color::Blue => 0x1,
            Color::Green => 0x2,
            Color::Cyan => 0x3,
            Color::Red => 0x4,
            Color::Magenta => 0x5,
            Color::Brown => 0x6,
            Color::Gray => 0x7,
            Color::DarkGray => 0x8,
            Color::BrightBlue => 0x9,
            Color::BrightGreen => 0xa,
            Color::BrightCyan => 0xb,
            Color::BrightRed => 0xc,
            Color::BrightMagenta => 0xd,
            Color::Yellow => 0xe,
            Color::White => 0xf,
        }
    }
}
