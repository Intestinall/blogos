use crate::color::Color;

pub fn write_to_vga_buffer(vga_buffer: *mut u8, message: &str, color: Color) -> *mut u8 {
    for (i, &byte) in message.as_bytes().iter().enumerate() {
        unsafe {
            *vga_buffer.offset(i as isize * 2) = byte;
            *vga_buffer.offset(i as isize * 2 + 1) = color.to_bytes();
        }
    };
    vga_buffer
}
